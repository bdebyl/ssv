package main

import "testing"

var errTmpl = "%s version failed to bump! given: %+v, expected: %+v"
var errMsgTmpl = "'%s' failed! given: %+v, expected %+v"

type semVerTest struct {
	in *SemVer
	pass bool
}

type parseVersionTest struct {
	in string
	out SemVer
	pass bool
}

type semVerMatchTest struct {
	in *SemVer
	out SemVer
	pass bool
}

type bumpVersionTest struct {
	msg string
	svt semVerMatchTest
}

type parseBumpTest struct {
	v string
	msg string
	sv semVerTest
}

var parseVersionTests = []parseVersionTest {
	{ "1.2.3", SemVer{1,2,3}, true },
	{ "", SemVer{}, true },
	{ "1.0.0", SemVer{1,0,0}, true },
	{ "1,0.0", SemVer{1,0,0}, false },
	{ "", SemVer{1,0,0}, false },
	{ "1.2.3.4", SemVer{}, false },
}

var semVerTests = []semVerTest {
	{ &SemVer{1,0,0}, true },
	{ &SemVer{}, true },
	{ nil, false },
}

var bumpMajorTests = []semVerMatchTest {
	{ nil, SemVer{}, false },
	{ &SemVer{0,0,0}, SemVer{1,0,0}, true },
	{ &SemVer{2,50,1}, SemVer{3,0,0}, true },
	{ &SemVer{999,124,0}, SemVer{1000,0,0}, true },
}

var bumpMinorTests = []semVerMatchTest {
	{ nil, SemVer{}, false },
	{ &SemVer{0,0,0}, SemVer{0,1,0}, true },
	{ &SemVer{3,50,1}, SemVer{3,51,0}, true },
	{ &SemVer{1010,124,55}, SemVer{1010,125,0}, true },
}

var bumpPatchTests = []semVerMatchTest {
	{ nil, SemVer{}, false },
	{ &SemVer{0,0,0}, SemVer{0,0,1}, true },
	{ &SemVer{3,50,1}, SemVer{3,50,2}, true },
	{ &SemVer{444,0,505}, SemVer{444,0,506}, true },
}

var bumpVersionTests = []bumpVersionTest {
	{
		"nil #major",
		semVerMatchTest{nil, SemVer{}, false},
	}, {
		"nil default",
		semVerMatchTest{nil, SemVer{}, false},
	}, {
		"#major",
		semVerMatchTest{&SemVer{0,0,0}, SemVer{1,0,0}, true},
	}, {
		"#minor",
		semVerMatchTest{&SemVer{0,0,0}, SemVer{0,1,0}, true},
	}, {
		"#patch",
		semVerMatchTest{&SemVer{0,0,0}, SemVer{0,0,1}, true},
	}, {
		"#minor change but bump #major",
		semVerMatchTest{&SemVer{1,0,0}, SemVer{2,0,0}, true},
	}, {
		"#patch #major (first) #minor",
		semVerMatchTest{&SemVer{4,2,3}, SemVer{5,0,0}, true},
	}, {
		"#patch (only) MINOR major# # minor",
		semVerMatchTest{&SemVer{0,3,2}, SemVer{0,3,3}, true},
	}, {
		"(#minor) message oops #patch",
		semVerMatchTest{&SemVer{1,1,1}, SemVer{1,2,0}, true},
	}, {
		"(major) message oops no # (should patch; default)",
		semVerMatchTest{&SemVer{0,11,41}, SemVer{0,11,42}, true},
	},
}

var parseBumpTests = []parseBumpTest {
	{
		"",
		"",
		semVerTest{nil, false},
	}, {
		"0.0.1",
		"",
		semVerTest{&SemVer{0,0,2}, true},
	}, {
		"0.0.1",
		"#minor",
		semVerTest{&SemVer{0,1,0}, true},
	}, {
		"1.2.3.4",
		"",
		semVerTest{nil, false},
	}, {
		"1..0.1",
		"",
		semVerTest{nil, false},
	}, {
		".1.0.1",
		"",
		semVerTest{nil, false},
	},
}

func TestParseVersion(t *testing.T) {
	for _, test := range parseVersionTests {
		sv, err := parseVersion(test.in)
		if test.pass {
			if sv != test.out {
				t.Errorf(errMsgTmpl, test.in, test.out, sv)
			}
		} else {
			if err == nil {
				t.Errorf(errMsgTmpl, test.in, test.out, sv)
			}
		}
	}
}

func TestCheckSemVerStruct(t *testing.T) {
	for _, test := range semVerTests {
		err := checkSemVerStruct(test.in)
		if test.pass && err != nil {
			t.Errorf("Expected failure for %+v", test.in)
		}
	}
}

func TestBumpMajor(t *testing.T) {
	for _, test := range bumpMajorTests {
		bumpMajor(test.in)
		if test.pass && *test.in != test.out {
			t.Errorf(errTmpl, "Major", test.in, test.out)
		}
	}
}

func TestBumpMinor(t *testing.T) {
	for _, test := range bumpMinorTests {
		bumpMinor(test.in)
		if test.pass && *test.in != test.out {
			t.Errorf(errTmpl, "Minor", test.in, test.out)
		}
	}
}

func TestBumpPatch(t *testing.T) {
	for _, test := range bumpPatchTests {
		bumpPatch(test.in)
		if test.pass && *test.in != test.out {
			t.Errorf(errTmpl, "Patch", test.in, test.out)
		}
	}
}

func TestBumpVersion(t *testing.T) {
	for _, test := range bumpVersionTests {
		err := BumpVersion(test.svt.in, test.msg)
		if err == nil {
			if test.svt.pass && *test.svt.in != test.svt.out {
				t.Errorf(errMsgTmpl, test.msg, test.svt.in, test.svt.out)
			}
		}
		if err != nil && test.svt.pass {
			t.Error(err.Error())
		}
	}
}

func TestParseAndBump(t *testing.T) {
	for _, test := range parseBumpTests {
		sv, err := ParseAndBump(test.v, test.msg)
		if test.sv.pass && *test.sv.in != sv {
			t.Errorf(errMsgTmpl, test.msg, test.v, test.sv.in)
		}
		if err != nil && test.sv.pass {
			t.Error(err.Error())
		}
	}
}
