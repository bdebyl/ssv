#    ___________   __
#   / ___/ ___/ | / /
#  (__  |__  )| |/ /
# /____/____/ |___/
#
# Copyright 2019 Bastian de Byl

# command definitions
GO?=/usr/bin/go

# go package variables
PKG_NAME=ssv
COVER=coverage.out
BUILD_FLAGS=-o ${PKG_NAME}

# default target
build: ${PKG_NAME}
${PKG_NAME}: *.go
	$(GO) build ${BUILD_FLAGS}

pkg: ${PKG_NAME}.zip
${PKG_NAME}.zip: ${PKG_NAME}
	$(ZIP) "${PKG_NAME}.zip" "${PKG_NAME}"

test:
	$(GO) test -v -coverprofile="${COVER}"
.PHONY: test

cover: test
	$(GO) tool cover -html="${COVER}"
.PHONY: cover
