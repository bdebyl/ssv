package main

// SemVer stores the portions of a version to easily interface with
type SemVer struct {
	Major, Minor, Patch uint64
}

// SemVerRequest serves as an object to contain message (optional) to be used
// in determining which version to bump of the version passed in
type SemVerRequest struct {
	Message, Version string
}
