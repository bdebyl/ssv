# Simple Semantic Versioning
The goal of this project is to have a simple API to run requests against as part
of external CI pipelines. The need arose from wanting a pipeline for
auto-tagging/bumping version numbers without the additional, heavy dependencies
for such a simple feature.

Simple SemVer (SSV) is a simple semantic versioning API. Requests are sent to
`hostname/semver` via plaintext. The request must contain a version number, and
optionally message to indicate the type (i.e `#major`, `#minor`, `#patch`). The
API returns a "bumped" version string.

## Example `POST`
```bash
$ curl -d 'hello world this is my commit message #minor' -X POST localhost:8080/semver?v=1.2.3

# Response
1.3.0
```

# Issues
See the [Issues](https://gitlab.com/bdebyl/simple-semver/issues) to track the
features to be implemented along with requesting others.
