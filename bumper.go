package main

import (
	"fmt"
	"strconv"
	"strings"
)

// parseVersion pulls out major, minor, patch versions for a string passed in of
// the right form. A regular expression is used to do this, meaning that a
// string of anything with 3 numerics separated by dots will be matched
func parseVersion(v string) (SemVer, error) {
	var sv SemVer

	versionList := strings.Split(v, ".")
	err := testVersionParse(versionList)
	if err != nil {
		return sv, fmt.Errorf("failed to parse version: '%s'", v)
	}

	// use the split values to construct a SemVer object
	if len(versionList) == 3 {
		return SemVer{
			parseVersionUint(versionList, 0),
			parseVersionUint(versionList, 1),
			parseVersionUint(versionList, 2),
		}, nil
	}
	return sv, fmt.Errorf("invalid version format: '%s'", v)
}

// testVersionParse attempts to parse the version into a uint, returning nil on
// success
func testVersionParse(versionList []string) (error) {
	for _, elem := range versionList{
		_, err := strconv.ParseUint(elem, 10, 64)
		if err != nil {
			return err
		}
	}
	return nil
}

// parseVersionUint converts a string to an unsigned int for SemVer struct usage
func parseVersionUint(versionList []string, index int) (uint64){
	// err from ParseUint can be discarded as this is covered by
	// testVersionParse
	val, _ := strconv.ParseUint(versionList[index], 10, 64)
	return val
}

// checkSemVerStruct is a helper for checking if the SemVer struct passed in is
// empty (nil)
func checkSemVerStruct(sv *SemVer) (error) {
	if sv != nil {
		return nil
	}

	return fmt.Errorf("invalid SemVer struct: %v", sv)
}

// bumpMajor bumps the major version by reference of a SemVer object
func bumpMajor(sv *SemVer) (error) {
	err := checkSemVerStruct(sv)
	if err != nil {
		return err
	}
	sv.Major++
	sv.Minor, sv.Patch = 0, 0

	return nil
}

// bumpMinor bumps the minor version by reference of a SemVer object
func bumpMinor(sv *SemVer) (error) {
	err := checkSemVerStruct(sv)
	if err != nil {
		return err
	}
	sv.Minor++
	sv.Patch = 0

	return nil
}

// bumpPatch bumps the patch version by reference of a SemVer object
func bumpPatch(sv *SemVer) (error) {
	err := checkSemVerStruct(sv)
	if err != nil {
		return err
	}
	sv.Patch++

	return nil
}

// BumpVersion determines which portion of version to bump (default patch)
func BumpVersion(sv *SemVer, msg string) (error) {
	err := new(error)
	switch {
	case strings.Contains(msg, "#major"):
		*err = bumpMajor(sv)
	case strings.Contains(msg, "#minor"):
		*err = bumpMinor(sv)
	case strings.Contains(msg, "#patch"):
		*err = bumpPatch(sv)
	default:
		*err = bumpPatch(sv)
	}

	if *err != nil {
		return *err
	}
	return nil
}

// ParseAndBump attempts to parse the version string into a SemVer struct which
// is returned upon success
func ParseAndBump(v string, msg string) (SemVer, error) {
	sv, err := parseVersion(v)
	if err != nil {
		return sv, err
	}
	BumpVersion(&sv, msg)

	return sv, nil
}
