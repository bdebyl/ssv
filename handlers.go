package main

import (
	"fmt"
	"github.com/aws/aws-lambda-go/events"
)

// SemVerHandler handles only a POST request made using required URL query
// parameters (e.g. '?v=1.2.3')
func SemVerHandler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	svr := SemVerRequest{}

	if v, ok := req.QueryStringParameters["v"]; ok {
		svr.Version = v
	} else {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body: "version number not given (e.g. ?v=1.2.3)",
		}, nil
	}

	// expecting commit message w/ keywords (e.g. "#patch", etc.) in request
	// body
	if len(req.Body) > 0 {
		svr.Message = req.Body
	}

	sv, err := ParseAndBump(svr.Version, svr.Message)
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Body: err.Error(),
		}, nil
	}

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body: fmt.Sprintf("%d.%d.%d", sv.Major, sv.Minor, sv.Patch),
	}, nil
}
